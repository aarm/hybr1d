import React from 'react';


const SearchInput = (props) => {

    return <div className="input-group mb-3">
        <input onChange={(e) => props?.handleSearch(e)} type="text" className="form-control" placeholder="search....." aria-label="search"
               aria-describedby="basic-addon2"/>
            <div className="input-group-append">
                {/*<button className="btn btn-outline-secondary" type="button">Search</button>*/}
            </div>
    </div>
}

export default SearchInput;