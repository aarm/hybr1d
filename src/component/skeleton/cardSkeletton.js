import React from 'react';
import { Skeleton } from 'antd';

const CardSkeleton = () => {

    return <>
        <Skeleton paragraph={{ rows: 4 }} />
    </>
}
export default CardSkeleton;