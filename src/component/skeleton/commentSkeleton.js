import React from 'react';
import { Skeleton } from 'antd';

const CommentSkeleton = () => {

    return <>
        <Skeleton avatar paragraph={{ rows: 4 }} />
    </>
}
export default CommentSkeleton;