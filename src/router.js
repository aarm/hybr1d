import React from "react";
import {
    BrowserRouter as Router,
    Routes,
    Route,
    Link
} from "react-router-dom";
import Home from "./pages/home/home";
import NewsDetails from "./pages/NewsDetails/newsDetails";

export default function AppRouter() {
    return (
        <Router>
            <div>
                <Routes>
                    <Route path="/home" element={<Home />}/>
                    <Route path="/item/:id" element={<NewsDetails/>}/>
                    <Route path="/" element={<Home />}/>
                </Routes>
            </div>
        </Router>
    );
}