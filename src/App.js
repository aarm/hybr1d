import React from "react";
import './App.css';
import AppRouter from "./router";


function App() {
  return (
    <div className="App p-3">
     <AppRouter/>
    </div>
  );
}

export default App;
