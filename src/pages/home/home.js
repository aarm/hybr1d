import React, {useState} from 'react';
import SearchInput from "../../component/SearchInput/searchInput";
import {useQuery} from 'react-query'
import axios from 'axios'
import {useNavigate} from 'react-router-dom'
import CardSkeleton from "../../component/skeleton/cardSkeletton";
import { debounce } from "lodash";

const Home = () => {
    const navigate = useNavigate();
    const [searchTerm, setSearchTerm] = useState('')


    /**
     * Function to fetch the news headline
     * @returns {Promise<*>}
     */
    async function fetchPosts(){
        const result = await axios.get(`https://hn.algolia.com/api/v1/search?query=${searchTerm}`)
        return result.data.hits
    }

    const {data, error, isError, isLoading } = useQuery(['posts', searchTerm], fetchPosts)

    /**
     * Function to set the search input
     * @returns {Promise<*>}
     */
    const handleSearch = (e) => {
        setSearchTerm(e.target.value)
    }

    const debounced = React.useCallback(debounce(handleSearch, 500), []);

    return <>
     <SearchInput handleSearch={debounced}/>
        <div className='container'>

            {/*/// IF DATA IS LOADING*/}
            {isLoading ? <div className='row'>
                {
                    Array(9).fill(0)?.map((value, index) => {
                        return <div className='col-md-4'>
                                <CardSkeleton/>
                            </div>
                    })
                }
            </div>  :null}

            {/*/// IF THERE ARE ANY ERROR*/}
            {isError ? <div>Error! {error.message}</div>  :null}

            {/*//// NEWS CARD UI*/}
            <div className='row'>
                    {
                        data && data?.length ? data?.map((item, index) => {
                            return  <div style={{cursor: 'pointer'}} className='col-md-4 ' role="button" onClick={() =>  {navigate(`/item/${item?.objectID}`)}}>
                                <div className='p-2 border mt-2' style={{borderRadius: '6px', height: '150px'}}>
                                    <div className='tags d-flex justify-content-end'>
                                        {item?._tags && item?._tags.length ? <div style={{width: '50px',color: '#a9d29d', padding: '4px', border: '1px solid #a9d29d', fontSize: '12px',   borderRadius: '20px'  }} className='ml-1 text-center'>{item?._tags[0]}</div> : null}
                                    </div>
                                    <div style={{fontWeight: '600', fontSize: '15px'}}>
                                        {item?.title?.length > 150 ? `${item?.title.substring(0, 150)}...` : item?.title}
                                    </div>
                                    <div style={{fontSize: '12px', color: '#A5A3A9'}}>Create at: {item?.created_at?.split('T')[0]}</div>
                                    <div style={{fontSize: '12px', color: '#A5A3A9'}}>Author: {item?.author}</div>
                                </div>
                            </div>
                        }) : null
                    }
            </div>

        </div>
    </>
}

export default Home;