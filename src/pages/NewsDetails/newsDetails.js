import React from 'react';
import {useParams} from "react-router-dom";
import axios from "axios";
import {useQuery} from "react-query";
import CommentSkeleton from "../../component/skeleton/commentSkeleton";

const NewsDetails = () => {
    const { id } = useParams();


    /**
     * Function to fetch the news details
     * @returns {Promise<*>}
     */
    async function fetchDetails(){
        const result = await axios.get(`https://hn.algolia.com/api/v1/items/${id}`)
        return result.data
    }

    const {data, error, isError, isLoading } = useQuery('details', fetchDetails)


    const Child = (dat) => {
        return <>
            {dat && dat?.map((item, index) => (
                <>
                    <div className='col-md-2 pr-0'>
                        <img style={{height: '40px'}} className="rounded-circle comment-img"
                             src={`https://via.placeholder.com/128/ffcc99/ff8000.png?text=${item?.author?.substring(0, 1)}`}/>
                    </div>
                    <div className='col-md-10 pl-0'>
                        <div className="mb-1"><a href="#" className="fw-bold link-dark pe-1">{item?.author}</a> <span
                            className="text-muted text-nowrap">Point: {item?.points ?? 0}</span></div>
                        <div className="mb-2" dangerouslySetInnerHTML={{__html: item?.text}}>
                        </div>
                    </div>
                    {item?.children?.length ? <Child dat={item?.children}/> : null}
                </>
            ))}
        </>
    }

    const Comment = (dat) => {
        return (
            <React.Fragment>
                {dat && dat?.dat?.children?.map((item, index) => (
                    <>
                        <div className='col-md-2 pr-0'>
                            <img style={{height: '40px'}} className="rounded-circle comment-img"
                                 src={`https://via.placeholder.com/128/ffcc99/ff8000.png?text=${item?.author?.substring(0, 1)}`}/>
                        </div>
                        <div className='col-md-10 pl-0'>
                            <div className="mb-1"><a href="#" className="fw-bold link-dark pe-1">{item?.author}</a> <span
                                className="text-muted text-nowrap">Point: {item?.points ?? 0}</span></div>
                            <div className="mb-2" dangerouslySetInnerHTML={{__html: item?.text}}>
                            </div>
                        </div>
                        {item?.children?.length ? <child dat={item?.children}/> : null}
                    </>
                ))}
            </React.Fragment>
        );
    };

    return <>
     <div className='row comment'>
         <div className='col-md-3'></div>
         <div className='col-md-6'>
             {isLoading ? <div className='row'>
                 {
                     Array(6).fill(0)?.map((value, index) => {
                         return <div className='col-md-12'><CommentSkeleton/></div>
                     })
                 }
             </div>  :null}
             {isError ? <div>Error! {error.message}</div>  :null}

             {
                 data && Object.keys(data).length ? <>
                     <h2>{data?.title}</h2>
                     <div style={{fontSize: '12px', color: '#A5A3A9'}}>Create at: {data?.created_at?.split('T')[0]}</div>
                     <div style={{fontSize: '12px', color: '#A5A3A9'}}>Author: {data?.author}</div>
                     <small className='d-flex justify-content-end'><a href={data?.url}>Read more >>>></a></small>
                     <div className='row mt-2'>
                         <Comment dat={data}/>
                     </div>
                 </> : null
             }
         </div>
         <div className='col-md-3'></div>
     </div>
    </>
}

export default NewsDetails;